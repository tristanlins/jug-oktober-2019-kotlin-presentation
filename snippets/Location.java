import lombok.Data;

import java.math.BigDecimal;

@Data
public class Location {

  private String title;

  private String address;

  private Geo geo;

  @Data
  public static class Geo {

    private BigDecimal longitudo;

    private BigDecimal latitudo;

  }

}

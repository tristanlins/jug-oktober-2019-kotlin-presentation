package lambdas;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lambdas {

  public static void example1() {
    final List<String> keys = Stream
      .of("Charlie", "Snoopy", "Woodstock")
      .map(String::toLowerCase)
      .collect(Collectors.toList());
  }

  public static void example2() {
    final List<String> keys = Stream
      .of("Charlie", "Snoopy", "Woodstock")
      .map(name -> name.toLowerCase())
      .collect(Collectors.toList());
  }

  public static void example3() {
    final List<String> surnames = Stream
      .of("Charlie Brown", "Lucy van Pelt")
      .map(name -> {
        final String[] parts = name.split(" ", 2);
        return parts[1];
      })
      .collect(Collectors.toList());
  }

}

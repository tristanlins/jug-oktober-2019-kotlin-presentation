package lambdas

fun example1() {
  val keys = listOf("Charlie", "Snoopy", "Woodstock")
    .map(String::toLowerCase)
}

fun example2() {
  val keys = listOf("Charlie", "Snoopy", "Woodstock")
    .map { name -> name.toLowerCase() }
}

fun example3() {
  val surnames = listOf("Charlie Brown", "Lucy van Pelt")
    .map { name ->
      val parts = name.split(" ", limit = 2)
      parts.last()
    }
}

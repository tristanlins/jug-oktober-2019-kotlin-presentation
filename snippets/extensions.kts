fun String.capitalizeWords(): String {
  return this.split(" ")
    .joinToString(" ") { it.capitalize() }
}

val message = "Every word will be capitalized"
  .capitalizeWords()
// -> Every Word Will Be Capitalized

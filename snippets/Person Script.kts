val `your speaker` = Person(
  name = "Tristan \"tril\" Lins",

  job = "DevOps Engineer and Software Architect"
)

`your speaker`.bio = """
  Developer since the early 2000s
    My first PC was the 1997 released Aldi PC, 
    with 166 MHz Pentium CPU
  
  Since 2015 at Chamaeleon AG, Montabaur
    ​Was previously over 8 years self-employed
""".trimIndent()

`your speaker`.participations = """
  |Contributor to numerous open source projects
  |
  |  Was active for over 6 years in the Contao CMS 
  |  and former member of the core development team.
  |  
  |  Contributed the Kotlin support to the  
  |  graphql-code-generator project.
  |  
  |  Founder and Core Developer of the jsass library.
  |  
  |  And some other projects.
""".trimMargin("|")

`your speaker`.favoritePet = "Cats \uD83D\uDC31"

`your speaker`.links += listOf(
  "Gitlab" to "https://gitlab.com/tristanlins",
  "Github" to "https://github.com/tristanlins",
  "Twitter" to "https://twitter.com/tristanlins",
  "Medium" to "https://medium.com/@tristan.lins",
  "Website" to "https://tristan.lins.io"
)

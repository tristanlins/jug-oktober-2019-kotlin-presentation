package hello

val immutable = "I am immutable!"
var mutable = "I am mutable!"

const val anInteger = 1
const val anotherInteger: Int = 1

var nullAllowed: String? = "A nullable variable"
var nullableInteger: Int? = null

// var compilerError: Int = null

val listOfStrings: List<String> = listOf(
  "A list of string (but not null!)"
)
val listOfNonNull: List<Any> = listOf(
  "Any non-null values"
)
val listOfWhatever: List<Any?> = listOf(
  "Add whatever you want!"
)
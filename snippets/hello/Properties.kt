package hello

class Aircraft {
  var title: String = ""

  var weight: Double = .0
    private set

  var friction: Double = .0
    set(value) {
      if (value < 0) throw IllegalArgumentException()
      field = value
    }

  private var engineType: String = "..."

  val airResistence: Double
    get() = weight * friction
}
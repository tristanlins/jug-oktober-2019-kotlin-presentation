package hello

class Greeter(val name: String) {
  fun greet() {
    println("Hello, $name")
  }
}

class Greeter2(name: String) {
  val name = name.capitalize()

  fun greet() {
    println("Hello, $name")
  }
}

class Greeter3(name: String) {
  val name: String

  init {
    this.name = name.capitalize()
  }

  fun greet() {
    println("Hello, $name")
  }
}

class Greeter4(val name: String) {
  constructor(): this("Charlie Brown")

  fun greet() {
    println("Hello, $name")
  }
}

fun main(args: Array<String>) {
  Greeter(args[0]).greet()
}

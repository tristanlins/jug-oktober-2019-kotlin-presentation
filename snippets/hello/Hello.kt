package hello

class Hello {
  fun hello() {
    println("Hello World")
  }
}

fun main() {
  Hello().hello()
}

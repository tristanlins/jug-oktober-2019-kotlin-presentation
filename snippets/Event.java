import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Event {

  private String title;

  private String description;

  private Location location;

  private Set<Person> participants = new HashSet<>();

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Set<Person> getParticipants() {
    return participants;
  }

  public void setParticipants(Set<Person> participants) {
    this.participants = participants;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Event)) return false;
    Event event = (Event) o;
    return Objects.equals(title, event.title) &&
      Objects.equals(description, event.description) &&
      Objects.equals(location, event.location) &&
      Objects.equals(participants, event.participants);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, description, location, participants);
  }

  @Override
  public String toString() {
    return "Event{" +
      "title='" + title + '\'' +
      ", description='" + description + '\'' +
      ", location=" + location +
      ", participants=" + participants +
      '}';
  }

}

data class Person(
  val name: String,
  var job: String = "",
  var bio: String = "",
  var favoritePet: String = "",
  val links: MutableList<Pair<String, String>> = mutableListOf(),
  var participations: String = ""
)

import java.awt.Point

fun example1() {
  fun Point.moveX(distance: Int): Point {
    return Point(x + distance, 0)
  }

  var point = Point(10, 4)
  point = point.moveX(5)
  // point -> (15, 4)
}

fun example2() {
  infix fun Point.moveY(distance: Int): Point {
    return Point(x, y + distance)
  }

  var point = Point(10, 4)
  point = point moveY 4
  // point -> (10, 8)
}

fun example3() {
  operator fun Point.plus(addition: Point): Point {
    return Point(x + addition.x, y + addition.y)
  }

  var point = Point(10, 4)
  point = point + Point(5, 4)
  // point -> (15, 8)
}

fun example4() {
  class Coordinates {
    fun addPoint(point: Point) {}

    operator fun invoke(body: Coordinates.() -> Unit) {
      body()
    }
  }

  val coordinates = Coordinates()

  coordinates {
    addPoint(Point(10, 4))
    addPoint(Point(15, 4))
    addPoint(Point(10, 8))
  }

  coordinates.addPoint(Point(15, 8))
}
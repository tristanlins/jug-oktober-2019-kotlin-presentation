data class Kotlin(
  val founder: String,
  val license: String,
  val url: String,
  val history: String,
  val abstract: String,
  val personalOpinion: String
)

val kotlin = Kotlin(
  founder = "Jet Brains",

  license = "Apache 2 license",

  url = "https://kotlinlang.org/",

  history = """
Introduced in 2011 as a new JVM language.
Since 2012, the source code is under the Apache 2 license.
Version 1.0 was released in 2016.
Since 2017 Kotlin gets first-class support on Android.
Since 2019 Kotlin is the preferred language for Android.
Version 1.3 is the currently stable version.
  """,

  abstract = """
Kotlin is a <mark>cross-platform</mark>, 
  <mark>statically types</mark>, 
  <mark>general-purpose programming language</mark>.

Kotlin is strongly inspired by Scala 
  and designed to interoperate fully with Java.
""",

  personalOpinion = """
Kotlin combines the advantages of object-oriented 
  and functional programming. 

Turns well-known best-practice approaches as an 
  essential part of the language, 
  avoiding unnecessary boilerplate code.
"""
)
